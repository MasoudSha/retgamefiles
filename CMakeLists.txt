cmake_minimum_required(VERSION 3.15)
project(LibHelloRunner)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin/)


find_package( OpenCV 4.0 REQUIRED COMPONENTS core imgproc calib3d highgui objdetect)
if(${OpenCV_FOUND})
    MESSAGE("OpenCV information:")
    MESSAGE("  OpenCV_INCLUDE_DIRS: ${OpenCV_INCLUDE_DIRS}")
    MESSAGE("  OpenCV_LIBRARIES: ${OpenCV_LIBRARIES}")
    MESSAGE("  OpenCV_LIBRARY_DIRS: ${OpenCV_LINK_DIRECTORIES}")
else()
    MESSAGE(FATAL_ERROR "OpenCV not found in the system.")
endif()

include_directories( ${OpenCV_INCLUDE_DIRS} )


# Move OpenCV
#file(GLOB files "lib/3rdParty/OpenCV/classifiers/*.xml")
file(GLOB files "D:/PROGRAMS/opencv41/prebuild/build/classifiers/*.xml")
foreach(file ${files})
    file(COPY ${file} DESTINATION ${CMAKE_BINARY_DIR}/bin/classifiers)
    install(FILES ${file} DESTINATION ${CMAKE_CONFIG_DIR}/classifiers)
endforeach()


add_executable(LibHelloRunner main.cpp)

target_link_libraries( LibHelloRunner ${OpenCV_LIBS} )

#TARGET_LINK_LIBRARIES(LibHelloRunner ${OpenCV})