#include <iostream>
#include <opencv2/opencv.hpp>



int main() {


    //cv::VideoCapture cap(0);
    cv::VideoCapture cap("../../IMG_0072_right.MOV");
    if (!cap.isOpened())
    {
        std::cerr << "Unable to connect to camera" << std::endl;
        return 1;
    }
    cap.set(cv::CAP_PROP_FRAME_WIDTH, 1280);          // working
    cap.set(cv::CAP_PROP_FRAME_HEIGHT, 720);         // working
    int count = 0;

    // Grab and process frames until the main window is closed by the user.
    double t = (double)cv::getTickCount();
    double fps = 60.0; // Just a place holder. Actual value calculated after 100 frames.
    cv::Mat im, im_preview;

        cap >> im;
        im_preview = im.clone();
    cv::imwrite("test.jpg",im_preview);
    std::cout <<" reading successful.\n";


//    while (cap.isOpened()) {
//        if (count == 0)
//            t = cv::getTickCount();
//        //Grab a frame
//        cap >> im;
//        im_preview = im.clone();
//


        // Uncomment the line below to see FPS
//        cv::putText(im_preview, cv::format("fps %.2f", fps), cv::Point(50, 50), cv::FONT_HERSHEY_COMPLEX, 1.0,
//                    cv::Scalar(0, 0, 255), 2);

        // Display result
//        cv::imshow("output", im_preview);
        //cv::imshow("stablized", stablized);


//        int k = cv::waitKey(1);
//        // Quit if 'q' or ESC is pressed
//        if (k == 'q' || k == 27) {
//            std::cout<<"fps: "<<fps<<"\n";
//            return fps;
//        }

//        count++;
//
//        if (count == 100) {
//            t = ((double) cv::getTickCount() - t) / cv::getTickFrequency();
//            fps = 100.0 / t;
//            count = 0;
//        }
//
//    }
 return 0;

}